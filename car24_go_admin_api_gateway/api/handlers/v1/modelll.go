package v1

import (
	"net/http"

	"gitlab.udevs.io/car24/car24_go_admin_api_gateway/modules/car24/response"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.udevs.io/car24/car24_go_admin_api_gateway/modules/car24/car24_car_service"

	cloudevents "github.com/cloudevents/sdk-go/v2"
)

// @Security ApiKeyAuth
// @Router /v1/modelll [post]
// @Summary Create Modelll
// @Description API for creating modelll
// @Tags modelll
// @Accept json
// @Produce json
// @Param Platform-Id header string true "platform-id" default(7d4a4c38-dd84-4902-b744-0488b80a4c01)
// @Param Modelll body car24_car_service.CreateModelll true "modelll"
// @Success 201 {object} response.ResponseOK
// @Failure 400 {object} response.ResponseError
// @Failure 500 {object} response.ResponseError
func (h *handlerV1) CreateModelll(c *gin.Context) {
	var modelll car24_car_service.CreateModelll

	err := c.ShouldBindJSON(&modelll)
	if HandleError(c, h.log, err, "error while binding body to model") {
		return
	}

	correlationID, err := uuid.NewRandom()
	if HandleError(c, h.log, err, "Error while generating new uuid") {
		return
	}

	id, _ := uuid.NewRandom()
	modelll.ID = id.String()
	e := cloudevents.NewEvent()
	e.SetID(uuid.New().String())
	e.SetSource(c.FullPath())
	e.SetType("v1.car_service.modelll.create")
	err = e.SetData(cloudevents.ApplicationJSON, modelll)
	if HandleError(c, h.log, err, "error while setting event data") {
		return
	}

	err = h.kafka.Push("v1.car_service.modelll.create", e)
	if HandleError(c, h.log, err, "error while publishing event") {
		return
	}

	c.JSON(http.StatusOK, response.ResponseOK{
		Message: correlationID.String(),
	})
}

// @Security ApiKeyAuth
// @Router /v1/modelll/{id} [get]
// @Summary Get Modelll
// @Description API for getting modelll
// @Tags modelll
// @Accept json
// @Produce json
// @Param Platform-Id header string true "platform-id" default(7d4a4c38-dd84-4902-b744-0488b80a4c01)
// @Param id path string true "id"
// @Success 200 {object} car24_car_service.Modelll
// @Failure 400 {object} response.ResponseError
// @Failure 500 {object} response.ResponseError
func (h *handlerV1) GetModelll(c *gin.Context) {
	_ = h.MakeProxy(c, h.cfg.CarServiceURL, c.Request.URL.Path)
}

// @Security ApiKeyAuth
// @Router /v1/modelll [get]
// @Summary Get modellls
// @Description API for getting all modellls
// @Tags modelll
// @Accept json
// @Produce json
// @Param find query car24_car_service.QueryParamModelll false "filters"
// @Success 200 {object} car24_car_service.ListModelll
// @Failure 400 {object} response.ResponseError
// @Failure 500 {object} response.ResponseError
func (h *handlerV1) GetAllModellls(c *gin.Context) {

	_ = h.MakeProxy(c, h.cfg.CarServiceURL, c.Request.URL.Path)
}

// @Security ApiKeyAuth
// @Router /v1/modelll/{id} [put]
// @Summary Update Modelll
// @Description API for updating modelll
// @Tags modelll
// @Accept json
// @Produce json
// @Param Platform-Id header string true "platform-id" default(7d4a4c38-dd84-4902-b744-0488b80a4c01)
// @Param id path string true "id"
// @Param Modelll body car24_car_service.UpdateModelll true "modelll"
// @Success 201 {object} response.ResponseOK
// @Failure 400 {object} response.ResponseError
// @Failure 500 {object} response.ResponseError
func (h *handlerV1) UpdateModelll(c *gin.Context) {
	var (
		modelll car24_car_service.UpdateModelll
	)

	err := c.ShouldBindJSON(&modelll)
	if HandleError(c, h.log, err, "error while binding body to model") {
		return
	}

	correlationID, err := uuid.NewRandom()
	if HandleError(c, h.log, err, "Error while generating new uuid") {
		return
	}

	modelll.ID = c.Param("id")

	e := cloudevents.NewEvent()
	e.SetID(uuid.New().String())
	e.SetSource(c.FullPath())
	e.SetType("v1.car_service.modelll.update")
	err = e.SetData(cloudevents.ApplicationJSON, modelll)
	if HandleError(c, h.log, err, "error while setting event data") {
		return
	}

	err = h.kafka.Push("v1.car_service.modelll.update", e)
	if HandleError(c, h.log, err, "error while publishing event") {
		return
	}

	c.JSON(http.StatusOK, response.ResponseOK{
		Message: correlationID.String(),
	})
}

// @Security ApiKeyAuth
// @Router /v1/modelll/{id} [delete]
// @Summary Delete Modelll
// @Description API for deleting modelll
// @Tags modelll
// @Accept json
// @Produce json
// @Param Platform-Id header string true "platform-id" default(7d4a4c38-dd84-4902-b744-0488b80a4c01)
// @Param id path string true "id"
// @Success 201 {object} response.ResponseOK
// @Failure 400 {object} response.ResponseError
// @Failure 500 {object} response.ResponseError
func (h *handlerV1) DeleteModelll(c *gin.Context) {
	var (
		request car24_car_service.DeleteModelll
	)

	correlationID, err := uuid.NewRandom()
	if HandleError(c, h.log, err, "Error while generating new uuid") {
		return
	}

	request.ID = c.Param("id")

	e := cloudevents.NewEvent()
	e.SetID(uuid.New().String())
	e.SetSource(c.FullPath())
	e.SetType("v1.car_service.modelll.delete")
	err = e.SetData(cloudevents.ApplicationJSON, request)
	if HandleError(c, h.log, err, "error while setting event data") {
		return
	}

	err = h.kafka.Push("v1.car_service.modelll.delete", e)
	if HandleError(c, h.log, err, "error while publishing event") {
		return
	}

	c.JSON(http.StatusOK, response.ResponseOK{
		Message: correlationID.String(),
	})
}
