package car24_car_service

type Modelll struct {
	ID          string `json:"id"`
	BrandID 	string `json:"brand_id"`
	Name		string `json:"name"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type CreateModelll struct {
	ID          string `json:"id"`
	BrandID 	string `json:"brand_id"`
	Name		string 	`json:"name"`
}

type UpdateModelll struct {
	ID          string `json:"id"`
	BrandID 	string `json:"brand_id"`
	Name		string `json:"name"`
}

type DeleteModelll struct {
	ID string `json:"id" swaggerignore:"true"`
}

type QueryParamModelll struct {
	Search string `json:"search"`
	Offset int    `json:"offset" default:"0"`
	Limit  int    `json:"limit" default:"10"`
}

type ListModelll struct {
	Modellls  []Modelll 		`json:"brands"`
	Count int       `json:"count"`
}
