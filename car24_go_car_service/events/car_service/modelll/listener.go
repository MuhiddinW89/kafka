package modelll

import (
	"context"
	"encoding/json"
	"net/http"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	cs "gitlab.udevs.io/car24/car24_go_car_service/modules/car24/car24_car_service"
	"gitlab.udevs.io/car24/car24_go_car_service/modules/car24/response"
	"gitlab.udevs.io/car24/car24_go_car_service/pkg/helper"
	"gitlab.udevs.io/car24/car24_go_car_service/pkg/logger"
)

func (c *ModelllService) Create(ctx context.Context, event cloudevents.Event) response.Response {
	var (
		modelll cs.CreateModelll
	)

	c.log.Info("Modelll create", logger.Any("event", event))

	err := json.Unmarshal(event.DataEncoded, &modelll)

	resp, hasError := helper.HandleBadRequest(c.log, modelll.ID, "Error while unmarshalling", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	_, err = c.storage.Modelll().Create(&modelll)

	resp, hasError = helper.HandleInternal(c.log, modelll.ID, "Error while creating modelll", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	c.log.Info("Successfully created Modelll", logger.String("id", modelll.ID))

	err = c.kafka.Push("v1.modelll_service.modelll.created", event)
	resp, hasError = helper.HandleInternal(c.log, modelll.ID, "Error while creating modelll", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	resp = response.Response{
		ID:         modelll.ID,
		StatusCode: http.StatusOK,
		Data:       modelll,
		Action:     "create",
	}

	return resp
}

func (c *ModelllService) Update(ctx context.Context, event cloudevents.Event) response.Response {
	var (
		modelll cs.UpdateModelll
	)

	c.log.Info("Modelll update", logger.Any("event", event))

	err := json.Unmarshal(event.DataEncoded, &modelll)
	resp, hasError := helper.HandleBadRequest(c.log, modelll.ID, "Error while marshaling", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	err = c.storage.Modelll().Update(&modelll)
	resp, hasError = helper.HandleInternal(c.log, modelll.ID, "Error while updating modelll", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	c.log.Info("Successfully Updated Modelll", logger.String("id", modelll.ID))

	err = c.kafka.Push("v1.modelll_service.modelll.updated", event)
	resp, hasError = helper.HandleInternal(c.log, modelll.ID, "Error while updating modelll", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	return response.Response{
		ID:         modelll.ID,
		StatusCode: http.StatusOK,
		Action:     "update",
	}
}

func (c *ModelllService) Delete(ctx context.Context, event cloudevents.Event) response.Response {
	var (
		modelll cs.DeleteModelll
	)

	c.log.Info("Modelll delete", logger.Any("event", event))

	err := json.Unmarshal(event.DataEncoded, &modelll)
	resp, hasError := helper.HandleInternal(c.log, modelll.ID, "Error while unmarshalling modelll", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	id := modelll.ID

	err = c.storage.Modelll().Delete(string(id))
	resp, hasError = helper.HandleInternal(c.log, modelll.ID, "Error while deleting modelll", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	c.log.Info("Successfully Deleted Modelll", logger.String("id", modelll.ID))

	err = c.kafka.Push("v1.modelll_service.modelll.deleted", event)
	resp, hasError = helper.HandleInternal(c.log, modelll.ID, "Error while deleting modelll", err)
	if hasError {
		resp.CorrelationID = modelll.ID
		return resp
	}

	return response.Response{
		ID:         modelll.ID,
		StatusCode: http.StatusOK,
		Action:     "delete",
	}
}
