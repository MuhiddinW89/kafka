package modelll

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/jmoiron/sqlx"
	"gitlab.udevs.io/car24/car24_go_car_service/config"
	"gitlab.udevs.io/car24/car24_go_car_service/pkg/event"
	"gitlab.udevs.io/car24/car24_go_car_service/pkg/logger"
	"gitlab.udevs.io/car24/car24_go_car_service/storage"
)
type ModelllService struct {
	cfg     config.Config
	log     logger.Logger
	storage storage.StorageI
	kafka   event.KafkaI
	bot     *tgbotapi.BotAPI
}

func New(cfg config.Config, log logger.Logger, db *sqlx.DB, kafka event.KafkaI) *ModelllService {
	return &ModelllService{
		cfg:     cfg,
		log:     log,
		storage: storage.NewStoragePg(db),
		kafka:   kafka,
	}
}

func (c *ModelllService) RegisterConsumers() {
	modelllRoute := "v1.modelll_service.modelll."

	c.kafka.AddConsumer(
		modelllRoute+"create", // consumer name
		modelllRoute+"create", // topic
		modelllRoute+"create", // group id
		c.Create,          // handlerFunction
	)

	c.kafka.AddConsumer(
		modelllRoute+"update", // consumer name
		modelllRoute+"update", // topic
		modelllRoute+"update", // group id
		c.Update,          // handlerFunction,
	)

	c.kafka.AddConsumer(
		modelllRoute+"delete", // consumer name
		modelllRoute+"delete", // topic
		modelllRoute+"delete", // group id
		c.Delete,          // handlerFunction,
	)

}
