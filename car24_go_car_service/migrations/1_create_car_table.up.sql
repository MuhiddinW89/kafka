create table if not exists brand  (
    "id" uuid primary key,
    "name" varchar not null,
    "created_at" timestamp default current_timestamp not null,
    "updated_at" timestamp default current_timestamp not null
);
-- insert into brand (id, name) values ('ba10623c-be8e-4e96-b54e-3bed14b89c80', 'DMC');
create table if not exists model  (
    "id" uuid primary key,
    "brand_id" UUID REFERENCES "brand" ("id")NOT NULL,
    "name" varchar not null,
    "created_at" timestamp default current_timestamp not null,
    "updated_at" timestamp default current_timestamp not null
);
--nsert into model (id, bran_id, name, created_at, updated_at) values ('ba10623c-be8e-4e96-b54e-3bed14b89c79','ba10623c-be8e-4e96-b54e-3bed14b89c80', 'De_Lorean_12' );

create table if not exists car (
    "id" uuid PRIMARY KEY,
    "model_id" UUID REFERENCES "model" ("id")NOT NULL,
    "category_id" uuid,
    "investor_id" uuid,
    "state_number" varchar,
    "updated_at" timestamp default current_timestamp not null,
    "created_at" timestamp default current_timestamp not null
);  


--Chevrolet -> Gentra -> 10 A 110 AA,
