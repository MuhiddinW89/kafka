package postgres

import (
	"database/sql"
	"fmt"

	cs "gitlab.udevs.io/car24/car24_go_car_service/modules/car24/car24_car_service"
	"gitlab.udevs.io/car24/car24_go_car_service/pkg/helper"
	"gitlab.udevs.io/car24/car24_go_car_service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type modelRepo struct {
	db *sqlx.DB
}

func NewModelll(db *sqlx.DB) repo.ModelllI {
	return &modelRepo{
		db: db,
	}
}

func (br *modelRepo) Create(model *cs.CreateModelll) (string, error) {
	tsx, err := br.db.Begin()
	if err != nil {
		return "", err
	}
	defer func() {
		if err != nil {
			_ = tsx.Rollback()
		} else {
			err := tsx.Commit()
			if err != nil {
				fmt.Println("While committing transaction ", err)
			}
		}
	}()

	query := `
	INSERT INTO 
		model(
			id,
			brand_id,
			name,
			updated_at
		)
	VALUES ($1, $2, $3, $4, now())`

	_, err = tsx.Exec( query,
		model.ID,
		model.BrandID,
		model.Name,
	)	

	if err != nil {
		return "", err
	}

	return model.ID, nil
}

func (br *modelRepo) Get(id string) (*cs.Modelll, error) {
	var (
		brandId     sql.NullString
		name 		sql.NullString
		updatedAt   sql.NullString
		createdAt   sql.NullString
	)

	query := `
		SELECT
			brand_id,
			name,
			updated_at,
			created_at
		FROM model
		WHERE id = $1
	`

	row := br.db.QueryRow(query, id)
	err := row.Scan(
		&brandId,
		&name,
		&updatedAt,
		&createdAt,
	)
	if err != nil {
		return nil, err
	}

	return &cs.Modelll{
		ID:          id,
		BrandID: 	 brandId.String	,	
		Name:        name.String,	
		UpdatedAt:   updatedAt.String,
		CreatedAt:   createdAt.String,
	}, nil
}

func (br *modelRepo) GetAll(queryParam cs.QueryParamModelll) (res cs.ListModelll, err error) {

	var (
		filter = "WHERE 1=1"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
		params = map[string]interface{}{}
		query  string
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand_id,
			name,
			updated_at,
			created_at
		FROM
			model
	`

	if queryParam.Offset > 0 {
		offset = " OFFSET :offset"
		params["offset"] = queryParam.Offset
	}

	if queryParam.Limit > 0 {
		limit = " LIMIT :limit"
		params["limit"] = queryParam.Limit
	}

	query += filter + offset + limit
	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := br.db.Query(query, args...)
	if err != nil {
		return res, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			brandId		sql.NullString
			name 		sql.NullString
			updatedAt   sql.NullString
			createdAt   sql.NullString
		)

		err := rows.Scan(
			&res.Count,
			&id,
			&brandId,
			&name,
			&updatedAt,
			&createdAt,
		)
		if err != nil {
			return res, err
		}

		res.Modellls = append(res.Modellls, cs.Modelll{
			ID:          id.String,
			BrandID: 	 brandId.String,
			Name:        name.String,	
			UpdatedAt:   updatedAt.String,
			CreatedAt:   createdAt.String,
		})
	}

	return res, nil
}

func (br *modelRepo) Update(model *cs.UpdateModelll) (err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE 
			    model
			SET	
				brand_id = :brand_id,
				name = :name,
				state_number = :state_number,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           model.ID,
		"brand_id": 	model.BrandID,
		"name":			model.Name,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	_, err = br.db.Exec(query, args...)
	if err != nil {
		return
	}

	return err
}

func (br *modelRepo) Delete(id string) error {
	tsx, err := br.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			_ = tsx.Rollback()
		} else {
			err := tsx.Commit()
			if err != nil {
				fmt.Println("While committing transaction ", err)
			}
		}
	}()

	query := `DELETE FROM model WHERE id=$1`
	_, err = tsx.Exec(query, id)

	if err != nil {
		return err
	}

	return err
}
